import math 
import pygame 

#Init
G  = 6.67428e-11
dt = 10000 # Speed
screen_x=1600
screen_y=1000
ratio=5e9 # scaling down the distances between the planets
border_x=800
border_y=500


# Define some colors
black    = (   0,   0,   0)
white    = ( 255, 255, 255)
green    = (   0, 255,   0)
red      = ( 255,   0,   0)
yellow   = ( 255,   255,   0)

 
pygame.init()
  
# Set the height and width of the screen
size = [screen_x, screen_y]
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("Planets")
 
#Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()

font = pygame.font.Font(None, 35)


def make_array(length):
	array=[]
	for a in range(len(length)):
		array.append([])
		for b in range(len(length)):
			array[a].append([])
	return array
	

def refreshPos(times):
	for a in range(len(my_planets)):
		if ( my_planets[a].pos_x/ratio)+(screen_x/2)>screen_x/2+border_x or (my_planets[a].pos_x/ratio)+(screen_x/2) <screen_x/2-border_x :
			my_planets.remove(my_planets[a])
			break
			
		if (my_planets[a].pos_y/ratio)+(screen_y/2) >screen_y/2+border_y or (my_planets[a].pos_y/ratio)+(screen_y/2) <screen_y/2-border_y :
			my_planets.remove(my_planets[a])
			break			
	
	for no_use in range(times):
		dis=[]
		dis=make_array(my_planets)
		
		for a in range(len(my_planets)):
			for b in range(len(my_planets)):
				dis[a][b]= math.sqrt( (my_planets[a].pos_x - my_planets[b].pos_x)**2 + (my_planets[a].pos_y - my_planets[b].pos_y)**2)
				


		F=[]
		F=make_array(my_planets)

		for a in range(len(my_planets)):
			for b in range(len(my_planets)):
				if dis[a][b]<3e11:
					F[a][b]=0
				elif dis[a][b] !=0:
					F[a][b]=G * my_planets[a].mass * my_planets[b].mass / dis[a][b]**2
					
			
		
		
		Fx=[]
		Fx=make_array(my_planets)
			
		for a in range(len(my_planets)):
			for b in range(len(my_planets)):
				if dis[a][b] !=0:
					Fx[a][b] = F[a][b] * (my_planets[b].pos_x - my_planets[a].pos_x) / dis[a][b]
			
			
		Fy=[]
		Fy=make_array(my_planets)
			
		for a in range(len(my_planets)):
			for b in range(len(my_planets)):
				if dis[a][b] !=0:
					Fy[a][b] = F[a][b] * (my_planets[b].pos_y - my_planets[a].pos_y) / dis[a][b]


		for a in range(len(my_planets)):
			my_planets[a].Fx_sum=0.0
			my_planets[a].Fy_sum=0.0
			for b in range(len(my_planets)):
				if Fx[a][b] !=[]:
					my_planets[a].Fx_sum=  my_planets[a].Fx_sum + Fx[a][b]

				if Fy[a][b] !=[]:
					my_planets[a].Fy_sum=  my_planets[a].Fy_sum + Fy[a][b]
			

		for a in range(len(my_planets)):
			my_planets[a].vel_x+=my_planets[a].Fx_sum/my_planets[a].mass*dt
			my_planets[a].vel_y+=my_planets[a].Fy_sum/my_planets[a].mass*dt
		
			my_planets[a].pos_x+=my_planets[a].vel_x*dt
			my_planets[a].pos_y+=my_planets[a].vel_y*dt
		


class Planet:
	def __init__(self,color,size, mass, pos_x, pos_y, vel_x, vel_y):
		self.color=color
		self.size=size
		self.mass=mass
		self.pos_x=pos_x
		self.pos_y=pos_y
		self.vel_x=vel_x
		self.vel_y=vel_y
		self.Fx_sum=0.0
		self.Fy_sum=0.0
		
 # ------- Giving the planets --------
my_planets=[]
#my_planets.append(Planet(yellow,10,1.98e30,0,0,0,0))# sun
my_planets.append(Planet(green,4,5.97e24,0,0,0,0)) # earth
my_planets.append(Planet(red,3,7.34e22,3.8e8,0,0,1e3)) # moon

state=0





 
# -------- Main Program Loop -----------
while done == False:
	for event in pygame.event.get(): # User did something
		if event.type == pygame.QUIT: # If user clicked close
			done = True # Flag that we are done so we exit this loop
			
		elif event.type==pygame.MOUSEBUTTONDOWN:
			if pygame.mouse.get_pressed()==(1,0,0):
				startPos=pygame.mouse.get_pos()
				state=1
				
		elif event.type==pygame.MOUSEBUTTONUP and state==1:
			endPos=pygame.mouse.get_pos()
			velocity_x= (-endPos[0]+startPos[0])*1e4
			velocity_y=(-endPos[1]+startPos[1])*1e4
			my_planets.append(Planet(red,3,27.30e33,(startPos[0]-screen_x/2)*ratio,(startPos[1]-screen_y/2)*ratio,velocity_x,velocity_y)) # moon
			state=0
	
	
		
				
			
 	# Set the screen background 
	screen.fill(black)    


	# Draw line when you shoot a planet
	if state==1:
		pygame.draw.line(screen, white, startPos,pygame.mouse.get_pos(),1)
 
    # Draw the planets
	for number in range(len(my_planets)):
		pygame.draw.circle(screen, my_planets[number].color, [int(my_planets[number].pos_x/ratio+screen_x/2), int(my_planets[number].pos_y/ratio+screen_y/2)], my_planets[number].size)
	
	
	text = font.render("Number of planets:", True, white)
	screen.blit(text, [0, 0])
	
	text = font.render(str(len(my_planets)), True, white)
	screen.blit(text, [230,0])
		
	# Refressing the planets positon
	refreshPos(1)
	
    # Limit to 500 frames per second
	clock.tick(500)
    
 
    # Go ahead and update the screen with what we've drawn.
	pygame.display.flip()
     
# Be IDLE friendly. If you forget this line, the program will 'hang'
# on exit.
pygame.quit()
