# Toy Planets Simulation

Toy example of simulating a multi body system, which is an
[n-body problem](https://en.wikipedia.org/wiki/N-body_problem). 
You can play around with gravitational slingshots,
gravity assist maneuvers or swing-by. 
![Screenshot](screenshot.png)
